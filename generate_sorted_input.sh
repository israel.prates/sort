#!/bin/bash

declare -ri length=${1}
declare -i i=0

[ ${length} -le 0 ] && {
	echo -e "Usage:\t${0} LENGTH" >&2
	exit
}

echo ${length}

while [ $((i++)) -lt ${length} ]; do
	echo ${i}
done
