# Copyright (C) 2018  Israel Felipe Prates
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

CC = gcc

HDR := $(wildcard *.h)
SRC := $(wildcard *.c)

# Common for both rules
# Target is set to heap-test since it is expected to be the first part being implemented (and tested).
TARGET = sort
CFLAGS = -std=c11 \
	-fstack-protector-all \
	-march=native -mavx2 \
	-W -Wall -Wextra \
	-Wbad-function-cast \
	-Wcast-align \
	-Wconversion \
	-Wfloat-equal \
	-Wformat-y2k \
	-Winit-self \
	-Winline \
	-Winvalid-pch \
	-Wmissing-declarations \
	-Wmissing-field-initializers \
	-Wmissing-format-attribute \
	-Wmissing-include-dirs \
	-Wmissing-noreturn \
	-Wmissing-prototypes \
	-Wnested-externs \
	-Wnormalized=nfc \
	-Wold-style-definition \
	-Woverlength-strings \
	-Wpacked \
	-Wredundant-decls \
	-Wshadow \
	-Wsign-compare \
	-Wstack-protector \
	-Wvolatile-register-var \
	-Wwrite-strings
INCLUDES = -I.
DEFINES =
LIBS =

# Specific for release rule
RELEASE_TARGET = $(TARGET)
RELEASE_CFLAGS = $(CFLAGS) \
	-O3
RELEASE_INCLUDES = $(INCLUDES)
RELEASE_DEFINES = $(DEFINES) -DNODEBUG
RELEASE_LIBS = $(LIBS)

# Specific for debug rule
DEBUG_TARGET = debug-$(TARGET)
DEBUG_CFLAGS = $(CFLAGS) \
	-g \
	-Og
DEBUG_INCLUDES = $(INCLUDES)
DEBUG_DEFINES = $(DEFINES) -DDEBUG
DEBUG_LIBS = $(LIBS)

#------------------------------------------------------------------------------
.PHONY: all release debug clean

#------------------------------------------------------------------------------
all: release debug

release: $(RELEASE_TARGET)
$(RELEASE_TARGET): $(SRC) $(HDR)
	$(CC) $(RELEASE_CFLAGS) $(RELEASE_DEFINES) $(RELEASE_INCLUDES) $(SRC) -o $@ $(RELEASE_LIBS)

debug: $(DEBUG_TARGET)
$(DEBUG_TARGET): $(SRC) $(HDR)
	$(CC) $(DEBUG_CFLAGS) $(DEBUG_DEFINES) $(DEBUG_INCLUDES) $(SRC) -o $@ $(DEBUG_LIBS)

#------------------------------------------------------------------------------
clean :
	$(RM) $(RELEASE_TARGET)
	$(RM) $(DEBUG_TARGET)
