/*
 * Copyright (C) 2018  Israel Felipe Prates
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stddef.h> // size_t

#include "sort.h"

void magic_function_1(int *data, size_t start, size_t end);

size_t magic_function_2(int *data, size_t start, size_t end);

void sort(array_t array) {
	if (array.length > 0) {
		magic_function_1(array.data, 0, array.length - 1);
	}
}

void magic_function_1(int *data, size_t start, size_t end) {
	size_t magic_number;

	if (start < end) {
		magic_number = magic_function_2(data, start, end);

		magic_function_1(data, start, (magic_number == 0) ? 0 : magic_number - 1);
		magic_function_1(data, magic_number + 1, end);
	}
}

size_t magic_function_2(int *data, size_t start, size_t end) {
	int magic_number = data[end];
	int temporary;
	size_t i = start;

	for (size_t j = start; j < end; j++) {
		if (data[j] <= magic_number) {
			temporary = data[i];
			data[i] = data[j];
			data[j] = temporary;
			i++;
		}
	}

	temporary = data[i];
	data[i] = data[end];
	data[end] = temporary;

	return i;
}
