/*
 * Copyright (C) 2018  Israel Felipe Prates
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

#include "array.h"

int read_array(FILE *stream, array_t *array) {

	/* Instruct GCC to ignore the unused-result warning. */
	#pragma GCC diagnostic push
	#pragma GCC diagnostic ignored "-Wunused-result"
	fscanf(stream, "%zu", &array->length);
	/* Restore normal behavior. */
	#pragma GCC diagnostic pop

	array->data = (int *) malloc(sizeof(int) * array->length);
	if (array->data == NULL) {
		return 0;
	}

	/* Instruct GCC to ignore the unused-result warning. */
	#pragma GCC diagnostic push
	#pragma GCC diagnostic ignored "-Wunused-result"
	for (size_t i = 0; i < array->length; i++) {
		fscanf(stream, "%d", &array->data[i]);
	}
	/* Restore normal behavior. */
	#pragma GCC diagnostic pop

	return !0;
}
