/*
 * Copyright (C) 2018  Israel Felipe Prates
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>

#include "array.h"
#include "sort.h"

int main(int argc, char **argv) {
	FILE *stream = NULL;
	array_t array;

	if (argc > 1) {
		stream = fopen(argv[1], "r");
		if (stream == NULL) {
			perror("Unable to open file with read permission. Aborting");
			exit(1);
		}
	}
	else {
		stream = stdin;
	}

	if (!read_array(stream, &array)) {
		perror("Unable to read input. Aborting");
		exit(1);
	}

	sort(array);

	for (size_t i = 0; i < array.length; i++) {
		printf("%d\n", array.data[i]);
	}

	return 0;
}
